package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.event.events.game.LoadWorldEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.module.settings.ValueBase;
import de.paxii.clarinet.util.settings.type.ClientSettingBoolean;

public class ModuleAutoLog extends Module {

  private boolean packetSent;

  public ModuleAutoLog() {
    super("AutoLog", ModuleCategory.OTHER);

    this.getModuleSettings().put("disableAfterQuit", new ClientSettingBoolean("Disable after quit", false));
    this.getModuleSettings().put("singleplayer", new ClientSettingBoolean("Singleplayer", false) {
      @Override
      public void onUpdate(Boolean newValue, Boolean oldValue) {
        ModuleAutoLog.this.packetSent = !newValue;
      }
    });
    this.getModuleValues().put("minimumHealth", new ValueBase("Minimum Health", 3F, 1F, 19F, true));

    this.setDescription("Automatically disconnects from the server if your health is lower or equal to the minimum health.");
    this.setVersion("1.0");
    this.setBuildVersion(19020);
    this.setRegistered(true);
  }

  @EventHandler
  public void onJoin(LoadWorldEvent worldEvent) {
    this.packetSent = false;

    if (!this.getValueOrDefault("singleplayer", Boolean.class, false)) {
      if (Wrapper.getMinecraft().isIntegratedServerRunning()) {
        this.packetSent = true;
      }
    }
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent tickEvent) {
    if (!this.packetSent
            && Wrapper.getPlayer().getHealth() <= this.getValueBase("minimumHealth").getValue()) {
      Wrapper.getWorld().sendQuittingDisconnectingPacket();
      this.packetSent = true;

      if (this.getValueOrDefault("disableAfterQuit", Boolean.class, false)) {
        this.setEnabled(false);
      }
    }
  }
}
